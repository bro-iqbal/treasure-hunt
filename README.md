# EVERMOS - TREASURE HUNT TEST

## CLONE PROJECT

You can clone this project to your local machine from this repository

```bash
git clone https://bro-iqbal@bitbucket.org/bro-iqbal/treasure-hunt.git
```

When it finished, run this command

```bash
go get
go run main.go
```

## HOW TO PLAY

Run this command to see how to use the CLI.

```bash
hunt
```

```bash
########
#......#
#.###..#
#...#.##
#X#....#
########
```

This is the available flogs to play:

| Flags   | Desc                                       |
| ------- | ------------------------------------------ |
| --up    | Flags to move player up [max step is 3]    |
| --right | Flags to move player right [max step is 5] |
| --down  | Flags to move player down [max step is 3]  |

### Tutorials

```sh
hunt start --up=1 --right=2 --down=1
```

You will move to this point

```bash
########
#......#
#.###..#
#...#.##
#.#X...#
########
```

To see the hints where the treasure is, run this command. The $ marker represents where the treasure

```bash
hunt hint
```
