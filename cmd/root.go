package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"

	"github.com/spf13/viper"
)

var cfgFile string
var up int
var down int
var right int

var rootCmd = &cobra.Command{
	Use:   "hunt",
	Short: "Treasure Hunt CLI in Go",
	Long:  `Treasure Hunt CLI in Go.`,
}

func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "0", "config file (default is $HOME/.hunt.yaml)")

	// rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")

	rootCmd.PersistentFlags().IntVar(&up, "up", 0, "Move count up max step 3")
	rootCmd.PersistentFlags().IntVar(&down, "down", 0, "Move count down max step 3")
	rootCmd.PersistentFlags().IntVar(&right, "right", 0, "Move count right max step 5")

}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := os.UserHomeDir()
		cobra.CheckErr(err)

		viper.AddConfigPath(home)
		viper.SetConfigType("yaml")
		viper.SetConfigName(".hunt")
	}

	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err == nil {
		fmt.Fprintln(os.Stderr, "Using config file:", viper.ConfigFileUsed())
	}
}
