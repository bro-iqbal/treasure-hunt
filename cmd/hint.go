package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var hintCmd = &cobra.Command{
	Use:   "hint",
	Short: "This command will show the treasure point",
	Long:  `This command will show the treasure point`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("########")
		fmt.Println("#......#")
		fmt.Println("#.###.$#")
		fmt.Println("#...#$##")
		fmt.Println("#X#$...#")
		fmt.Println("########")

	},
}

func init() {
	rootCmd.AddCommand(hintCmd)
}
