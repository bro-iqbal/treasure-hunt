package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var startCmd = &cobra.Command{
	Use:   "start",
	Short: "This command will start the game",
	Long:  `This command will start the game`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("########")

		switch up {
		case 1:
			switch right {
			case 1:
				switch down {
				case 1:
					fmt.Println("* You hit the obstacle *")
				case 2:
					fmt.Println("* You hit the obstacle *")
				case 3:
					fmt.Println("* You hit the obstacle *")
				default:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#.X.#.##")
					fmt.Println("#.#....#")
				}
			case 2:
				switch down {
				case 1:
					fmt.Println("* You are the Winner! You get the treasure *")
				case 2:
					fmt.Println("* You hit the obstacle *")
				case 3:
					fmt.Println("* You hit the obstacle *")
				default:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#..X#.##")
					fmt.Println("#.#....#")
				}
			case 3:
				fmt.Println("* You hit the obstacle *")
			case 4:
				fmt.Println("* You hit the obstacle *")
			case 5:
				fmt.Println("* You hit the obstacle *")
			default:
				switch down {
				case 1:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#...#.##")
					fmt.Println("#x#....#")
				case 2:
					fmt.Println("* You hit the obstacle *")
				case 3:
					fmt.Println("* You hit the obstacle *")
				default:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#X..#.##")
					fmt.Println("#.#....#")
				}
			}
		case 2:
			switch right {
			case 1:
				fmt.Println("* You hit the obstacle *")
			case 2:
				fmt.Println("* You hit the obstacle *")
			case 3:
				fmt.Println("* You hit the obstacle *")
			case 4:
				fmt.Println("* You hit the obstacle *")
			case 5:
				fmt.Println("* You hit the obstacle *")
			default:
				switch down {
				case 1:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#X..#.##")
					fmt.Println("#.#....#")
				case 2:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#...#.##")
					fmt.Println("#X#....#")
				case 3:
					fmt.Println("* You hit the obstacle *")
				default:
					fmt.Println("#......#")
					fmt.Println("#X###..#")
					fmt.Println("#...#.##")
					fmt.Println("#.#....#")
				}
			}
		case 3:
			switch right {
			case 1:
				fmt.Println("* You hit the obstacle *")
			case 2:
				fmt.Println("* You hit the obstacle *")
			case 3:
				fmt.Println("* You hit the obstacle *")
			case 4:
				switch down {
				case 1:
					fmt.Println("#......#")
					fmt.Println("#.###X.#")
					fmt.Println("#...#.##")
					fmt.Println("#.#....#")
				case 2:
					fmt.Println("* You are the Winner! You get the treasure *")
				case 3:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#...#.##")
					fmt.Println("#.#..X.#")
				default:
					fmt.Println("#....X.#")
					fmt.Println("#.###..#")
					fmt.Println("#...#.##")
					fmt.Println("#.#....#")
				}
			case 5:
				switch down {
				case 1:
					fmt.Println("* You are the Winner! You get the treasure *")
				case 2:
					fmt.Println("* You hit the obstacle *")
				case 3:
					fmt.Println("* You hit the obstacle *")
				default:
					fmt.Println("#.....X#")
					fmt.Println("#.###..#")
					fmt.Println("#...#.##")
					fmt.Println("#.#....#")
				}
			default:
				switch down {
				case 1:
					fmt.Println("#......#")
					fmt.Println("#X###..#")
					fmt.Println("#...#.##")
					fmt.Println("#.#....#")
				case 2:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#X..#.##")
					fmt.Println("#.#....#")
				case 3:
					fmt.Println("#......#")
					fmt.Println("#.###..#")
					fmt.Println("#...#.##")
					fmt.Println("#X#....#")
				default:
					fmt.Println("#X.....#")
					fmt.Println("#.###..#")
					fmt.Println("#...#.##")
					fmt.Println("#.#....#")
				}
			}
		default:
			fmt.Println("#......#")
			fmt.Println("#.###..#")
			fmt.Println("#...#.##")
			fmt.Println("#X#....#")
		}

		fmt.Println("########")

	},
}

func init() {
	rootCmd.AddCommand(startCmd)
}
